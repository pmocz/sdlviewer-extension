Xinyi Guo and Philip Mocz
CS 207
SDL viewer extension that allows visualization of shaded triangles and adding of listeners for interactivity.

== Example usage ==

// add triangles
auto node_map = viewer.empty_node_map(mesh);
viewer.add_nodes(mesh.node_begin(), mesh.node_end(), HeatMap(cmin,cmax), NodePosition(), node_map);
// Replace HeatMap() with your personal color functor as in HW4. The
color of the triangles is rendered based on the color of the nodes.
// Replace NodePosition() with your personal node position functor as in HW4.
viewer.add_triangles(mesh.triangle_begin(), mesh.triangle_end(), node_map);

// add listener
my_listener* l = new my_listener(viewer,mesh); 
viewer.add_listener(l);

// sample SDL listener
struct my_listener : public CS207::SDL_Listener {
  void handle(SDL_Event e) {   // we are forced to implement this function
    switch (e.type) {
      case SDL_MOUSEBUTTONDOWN: {
        if (e.button.button == SDL_BUTTON_RIGHT ) {
          // CODE INTERACTIVE STUFF HERE
        }
      } break;
    }
  }
  
  // constructor
  my_listener(CS207::SDLViewer& viewer, MeshType& mesh) : viewer_(viewer), mesh_(mesh) {};
  private:
   CS207::SDLViewer& viewer_;
   MeshType& mesh_;
};